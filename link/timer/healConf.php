<?php
function getConfFiles($dir)
{
    $files = array();
    $handle = opendir($dir);

    while (($file = readdir($handle)) !== false)
    {
        if ($file !== '.' && $file !== '..') {
            $filePath = $dir . '/' . $file;

            if (is_dir($filePath))
                $files = array_merge($files, getConfFiles($filePath));
            else
                $files[] = $filePath;
        }
    }

    closedir($handle);
    return $files;
}

function copyUserSettings(&$historyConfig, &$currentConfig,&$level=0)
{
    if (is_array($historyConfig) && is_array($currentConfig))
    {

        if($level == 0 && (count($historyConfig) != count($currentConfig)))
            return;

        foreach ($historyConfig as $key => $value) {
            if (is_array($value) && is_array($currentConfig[$key]) || is_object($value) && is_object($currentConfig[$key]))
            {
                $level++;
                copyUserSettings($value, $currentConfig[$key],$level);
            }
            else
                $currentConfig[$key] = $value;
        }
    }
    elseif (is_object($historyConfig) && is_object($currentConfig))
    {

        foreach ($historyConfig as $key => $value)
        {
            if (isset($currentConfig->$key))
            {
                if (is_array($value) && is_array($currentConfig->$key) || is_object($value) && is_object($currentConfig->$key))
                {
                    $level++;
                    copyUserSettings($value, $currentConfig->$key,$level);
                }
                else
                    $currentConfig->$key = $value;
            }
        }
    }
}

$configFiles = getConfFiles('/tmp/history_config');
foreach ($configFiles as $historyFile)
{
    $currentFile = str_replace("/tmp/history_config/","/link/config/",$historyFile);
    if(!strpos($currentFile, ".json") || md5_file($historyFile) == md5_file($currentFile))
        continue;

    $ignoreFiles = ['version.json', 'net.json', 'net2.json', 'netEx.json', 'netManager.json', 'verLogs.json', 'theme.json'];
    if (in_array(basename($currentFile), $ignoreFiles))
        continue;

    $historyCtx = json_decode(file_get_contents($historyFile));
    $currentCtx = json_decode(file_get_contents($currentFile));

    if(basename($currentFile) == "config.json")
    {
        $currentHadNdi = false;
        foreach ($currentCtx as $curObj)
        {
            if($curObj->type == 'ndi')
            {
                $currentHadNdi = true;
                break;
            }
        }
        $historyHadNdi = false;
        foreach ($historyCtx as $hisObj)
        {
            if($hisObj->type == 'ndi')
            {
                $historyHadNdi = true;
                break;
            }
        }
        if(!$currentHadNdi && $historyHadNdi)
        {
            $historyCtx = array_filter($historyCtx,function ($obj) {
                return $obj->type != 'ndi';
            });
            $historyCtx = array_values($historyCtx);
            foreach ($historyCtx as $index => $obj) {
                $obj->id = $index;
                if(isset($obj->stream))
                {
                    $obj->stream->suffix='stream'.$index;
                    $obj->stream2->suffix='sub'.$index;
                }
                if(isset($obj->enca))
                {
                    $obj->enca->audioSrc = $index;
                }
                if(isset($obj->ndi))
                {
                    $obj->ndi->name = 'stream'.$index;
                }

                if($obj->type == 'mix')
                {
                    if(intval($obj->output->src) >= count($historyCtx))
                        $obj->output->src = $index;

                    if(intval($obj->output2->src) >= count($historyCtx))
                        $obj->output2->src = $index;


                    $srcV = $obj->srcV;
                    $newSrcV =  [];
                    foreach ($srcV as $value) {
                        if (intval($value) < count($historyCtx))
                            $newSrcV[] = $value;
                        else
                            $newSrcV[] = "-1";
                    }
                    $obj->srcV = $newSrcV;

                    $srcA = $obj->srcA;
                    $newSrcA =  [];
                    foreach ($srcA as $value) {
                        if (intval($value) < count($historyCtx))
                            $newSrcA[] = $value;
                        else
                            $newSrcA[] = "-1";
                    }
                    $obj->srcA = $newSrcA;
                }
            }
        }
    }

    copyUserSettings($historyCtx,$currentCtx);

    if(empty($currentCtx))
        continue;

    file_put_contents($currentFile, json_encode($currentCtx, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    exec("sync");
}