<?php

namespace Link\Api;

use Link\Rpc\RpcClient;
use Exception;
class Record extends Verify
{
    function get_record_confs(): string
    {
        try {
            $this->link_verify();
            $record = $this->load_conf('/link/config/record.json');
            $format = $record['format'];
            $result = array(
                'chns' => $record['chns'],
                'mp4' => $format['mp4'],
                'flv' => $format['flv'],
                'mkv' => $format['mkv'],
                'mov' => $format['mov'],
                'ts' => $format['ts']
            );
            return $this->handleRet($result, "success", "执行完成", "execution is completed");
        } catch (Exception $ex) {
            return $this->handleRet('', 'error', $ex->getMessage(), $ex->getMessage());
        }
    }

    function set_record_confs($params): string
    {
        try {
            $this->link_verify();
            $params = json_decode($params,true);
            $this->check_args($params);

            $record = $this->load_conf('/link/config/record.json');
            $format = $record['format'];
            foreach ($params as $key => $value)
            {
                if($key == "chns")
                {
                    $record["chns"] = $value;
                }
                else
                {
                    if(!isset($format[$key]))
                        continue;
                    $format[$key] = $value;
                }
            }
            $record['format'] = $format;

            $client = new RpcClient();
            $client->update_record($record);

            return $this->handleRet('', "success", "执行完成", "execution is completed");
        } catch (Exception $ex) {
            return $this->handleRet('', 'error', $ex->getMessage(), $ex->getMessage());
        }
    }

    function start_rec(): string
    {
        try {
            $this->link_verify();

            $client = new RpcClient();
            $ret = $client->start_record();
            if(!$ret)
                return $this->handleRet('', "error", "没有找到外部存储设备", "no external storage device was found");
            return $this->handleRet('', "success", "执行完成", "execution is completed");
        } catch (Exception $ex) {
            return $this->handleRet('', 'error', $ex->getMessage(), $ex->getMessage());
        }
    }

    function stop_rec(): string
    {
        try {
            $this->link_verify();

            $client = new RpcClient();
            $client->stop_record();

            return $this->handleRet('', "success", "执行完成", "execution is completed");
        } catch (Exception $ex) {
            return $this->handleRet('', 'error', $ex->getMessage(), $ex->getMessage());
        }
    }
}